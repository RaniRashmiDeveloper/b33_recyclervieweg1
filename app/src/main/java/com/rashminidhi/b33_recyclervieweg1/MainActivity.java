package com.rashminidhi.b33_recyclervieweg1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    EditText mEditText1,mEditText2;
    RecyclerView mRecyclerView;
    ArrayList<Movies> mMoviesArrayList;
    MyRecyclerAdapter mMyRecyclerAdapter;
    Button mButton;
    int count = 0;

    //RECYCLER ADAPTER
    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder>{

        public class MyViewHolder extends RecyclerView.ViewHolder{

            public TextView tv1,tv2,tv3; //based on ur row.xml

            public MyViewHolder(View itemView) {
                super(itemView);
                //intialize variables...cpu heavy task
                tv1 = (TextView) itemView.findViewById(R.id.textView1);
                tv2 = (TextView) itemView.findViewById(R.id.textView2);
                tv3 = (TextView) itemView.findViewById(R.id.textView3);

            }
        }

        @Override
        public MyRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            //load row.xml
            View v = getLayoutInflater().inflate(R.layout.row,parent,false);  //remeber this for full row if u give true it will print in reverse order

            //pass row.xml to view holder and

            MyViewHolder myViewHolder = new MyViewHolder(v);

            //return view holder
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(MyRecyclerAdapter.MyViewHolder holder, int position) {

            //fill the data onto view holder(i.e....row.xml)
            //a. baswed on postion, get arraylist(SOURCE) data

            Movies movies = mMoviesArrayList.get(position);
            //b.fill the data  onto view holder

            holder.tv1.setText(movies.getSno());
            holder.tv2.setText(movies.getActor());
            holder.tv3.setText(movies.getMovie());


        }

        @Override
        public int getItemCount() {
            return mMoviesArrayList.size();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditText1 = (EditText) findViewById(R.id.editText1);
        mEditText2 = (EditText) findViewById(R.id.editText2);

        mButton = (Button)findViewById(R.id.button);
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mMoviesArrayList = new ArrayList<Movies>();
        mMyRecyclerAdapter = new MyRecyclerAdapter();
        //set link between adapter and recyler adapter

        mRecyclerView.setAdapter(mMyRecyclerAdapter);

        //prepare linear layout manager vertical which looks liske list layout

//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);

        //GridLayoutManager gridLayoutManager = new GridLayoutManager(this,3);  //3 is how many rows



        //set link between layout manager and recyler view
        //mRecyclerView.setLayoutManager(linearLayoutManager);

        //mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                count++;

                String actor = mEditText1.getText().toString();
                String movie =  mEditText2.getText().toString();

                Movies movies = new Movies();
                movies.setSno(" "+count);
                movies.setActor(actor);
                movies.setMovie(movie);

                mMoviesArrayList.add(movies);

                //tell to adapter
                mMyRecyclerAdapter.notifyItemInserted(mMoviesArrayList.size()-1);

                //clean edit text field
                mEditText1.setText("");
                mEditText2.setText("");

                mEditText1.requestFocus();
            }
        });

    }
}
