package com.rashminidhi.b33_recyclervieweg1;

/**
 * Created by RASHMI on 11/9/2016.
 */

public class Movies {

    String sno,actor,movie;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }
}
